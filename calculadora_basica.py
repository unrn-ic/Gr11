def calculadora_basica():
    opcion = float(input("""Ingrese una de las opciones para operar:
                1: Suma
                2: Resta
                3: Multiplicación
                4: División
                Opción: """))
    match opcion:
        case 1:
            num1 = float(input("Ingrese el primer numero para sumar: "))
            print("Si quiere finalizar escriba =")
            num2 = input("ingrese el segundo numero: ")
            while num2 != "=":
                num1 = num1 + float(num2)
                print(f"La suma es: {num1}")
                num2 = input("ingrese el nuevo numero o = para finalizar: ")
                print(f"la suma es {num1}") 
        case 2:
            num1 = float(input("Ingrese el primer numero para restar: "))
            print("Si quiere finalizar escriba =")
            num2 = input("ingrese el segundo numero: ")
            while num2 != "=":
                num1 = num1 - float(num2)
                print(f"La resta es: {num1}")
                num2 = input("ingrese el nuevo numero o = para finalizar: ")
                print(f"la resta es {num1}") 
        case 3:
            num1 = float(input("Ingrese el primer numero para multiplicar: "))
            print("Si quiere finalizar escriba =")
            num2 = input("ingrese el segundo numero: ")
            while num2 != "=":
                num1 = num1 * float(num2)
                print(f"La multiplicación es: {num1}")
                num2 = input("ingrese el nuevo numero o = para finalizar: ")
                print(f"la multiplicación es {num1}")
        case 4:
            num1 = float(input("Ingrese el primer numero para dividir: "))
            print("Si quiere finalizar escriba =")
            num2 = input("ingrese el segundo numero: ")
            while num2 != "=":
                num1 = num1 // float(num2)
                print(f"La división es: {num1}")
                num2 = input("ingrese el nuevo numero o = para finalizar: ")
                print(f"la división es {num1}")     
        case other:
            print("ingrese una opción válida.")
            calculadora_basica()