# Calculadora Digital - README

¡Bienvenido a la Calculadora Digital! Este proyecto es una simulación de una calculadora digital diseñada para ofrecer diversas funcionalidades a los usuarios. A continuación, se detallan las características principales de la calculadora y las opciones disponibles.

## Características

- Encendido y apagado: Para encender la calculadora, simplemente presiona el botón de activación (On). Del mismo modo, para apagarla, selecciona la opción "Salir" (Off).

- Operaciones unarias y binarias: La calculadora permite realizar operaciones tanto unarias como binarias. Las operaciones suma, resta, multiplicación y división son operaciones binarias, lo que significa que requieren dos operandos. Estas operaciones se escriben en notación infija, por ejemplo, "1 + 2" (con el operador en medio). Por otro lado, el operador "=" es unario, ya que evalúa la operación que aparece en pantalla y muestra el resultado.

- Menú de opciones: La calculadora cuenta con un menú de opciones que ofrece diferentes funcionalidades. Las opciones disponibles son las siguientes:
  1. Calculadora Clásica: Permite realizar operaciones con números enteros o reales.
  2. Calculadora de Fracciones: Permite realizar operaciones con fracciones.
  3. Calculadora de Conversiones: Permite convertir números enteros positivos de hasta 4 dígitos entre diferentes sistemas numéricos.
  4. Salir (Off): Permite finalizar la operación y apagar la calculadora.

## Uso de la Calculadora

1. Encendido y selección de opción: Una vez encendida la calculadora, revisa las opciones disponibles en el menú y selecciona la que se ajuste a tus necesidades.

2. Calculadora Clásica:
   - Selecciona la opción "Calculadora Clásica" en el menú.
   - Elige una operación entre "Suma", "Resta", "Multiplicación" o "División".
   - Ingresa los números en formato entero o real (con parte entera, punto y parte decimal).
   - Presiona el botón "=" para evaluar y mostrar el resultado en pantalla.
   - Si deseas realizar otra operación, regresa al paso anterior.

3. Calculadora de Fracciones:
   - Selecciona la opción "Calculadora de Fracciones" en el menú.
   - Elige una operación entre "Suma", "Resta", "Multiplicación" o "División".
   - Ingresa las fracciones en el formato adecuado.
   - Presiona el botón "=" para evaluar y mostrar el resultado en pantalla.
   - Si deseas realizar otra operación, regresa al paso anterior.

4. Calculadora de Conversiones:
   - Selecciona la opción "Calculadora de Conversiones" en el menú.
   - Elige una opción de conversión entre "Binario", "Hexadecimal" u "Octal".
   - Ingresa un número en formato decimal que cumpla con las restricciones indicadas.
   - Presiona el botón correspondiente ("bin", "hex" u "oct") para realizar la conversión y mostrar el resultado en pantalla.
   - Si deseas realizar otra conversión, regresa al paso anterior.

5. Apagado de la calculadora:
   - Para finalizar la operación y apagar la calculadora, selecciona la opción "Salir" (Off) en el menú.

¡Disfruta utilizando la Calculadora Digital y aprovecha al máximo todas sus funcionalidades! En caso de tener alguna duda o inconveniente, no dudes en consultar la documentación adicional proporcionada.

---

