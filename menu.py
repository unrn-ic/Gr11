from calculadora_basica import calculadora_basica
from conversion_numerica import conversion_numerica
from calculadora_fracciones import calculadora_fracciones

def menu():
    prender = input("para encender la calculadora ingrese la letra o: ")
    while prender == "o":
        print("opcion 1: calculadora clasica")
        print("opcion 2: calculadora de fracciones")
        print("opcion 3: conversion numerica")
        print("opcion 4: apagar")
        opcion = int(input("elija el numero de opcion: "))
        match opcion:
            case 1:   
                print("ha elegido la funcion de calculadora clasica")
                #funcion calculadora basica
                calculadora_basica()
            case 2:
                print("ha elegido la funcion de calculadora de fracciones")
                #funcion calculadora de fracciones
                calculadora_fracciones()
            case 3:
                print("ha elegido la funcion de conversion numerica")
                #funcion conversion numerica
                conversion_numerica()
            case 4: 
                print("Se ha finalizado el programa.")
                prender = ""
                break
            case other:
                print("ha elegido una opcion incorrecta")
    menu()
menu()