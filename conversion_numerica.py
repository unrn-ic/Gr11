def conversion_bin(num):
    numbin = ""
    while num > 0:
        mod = str(num % 2)
        numbin = numbin + mod
        num = num // 2
    numbin = numbin [::-1]
    return numbin

def conversion_oct(num):
    numoct = ""
    while num > 7:
        mod = str(num % 8)
        numoct = mod + numoct
        num = num // 8
    numoct = str(num) + numoct
    return numoct

def conversion_hex(num):
    numhex = ""
    hex_digits = "0123456789ABCDEF"
    while num > 0:
        mod = num % 16
        numhex = hex_digits[mod] + numhex
        num = num // 16
    return numhex

opcion = {
    "selecOpcion": "Seleccione la opción: ",
    "opcionSelec": "Seleccionaste la opción: ",
    "ingreseNum": "Ingrese el numero a convertir: "
}

def conversion_numerica():

    print("[1]: Convertir numero a BINARIO")
    print("[2]: Convertir numero a OCTAL")
    print("[3]: Convertir numero a HEXADECIMAL")

    opcionSelec = int(input(opcion["selecOpcion"]))

    numeroConver = ""

    match opcionSelec:
        case 1:
            print(opcion["opcionSelec"], opcionSelec)
            numero = int(input(opcion["ingreseNum"]))
            numeroConver = conversion_bin(numero)
        case 2:
            print(opcion["opcionSelec"], opcionSelec)
            numero = int(input(opcion["ingreseNum"]))
            numeroConver = conversion_oct(numero)
        case 3:
            print(opcion["opcionSelec"], opcionSelec)
            numero = int(input(opcion["ingreseNum"]))
            numeroConver = conversion_hex(numero)
        case other:
            conversion_numerica()
    print("La conversión numerica es: ", numeroConver)
