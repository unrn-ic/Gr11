def calculadora_fracciones():
    opcion = int(input("""elija una opcion:
    opcion 1: suma de fracciones
    opcion 2: resta de fracciones
    opcion 3: multiplicacion de fracciones
    opcion 4: division de fracciones
    """))
    match opcion:
        case 1:
            print("ha elegido suma de fracciones:")
            den_1 = input("ingrese el numerador de la fraccion: ")
            div_1 = input("ingrese el denominador de la fraccion: ")
            den_2 = input("ingrese el numerador de la otra fraccion: ")
            div_2 = input("ingrese el denominador de la otra fraccion: ")
            while den_2 != "=" or div_2 != "=":
                den_2 = int(den_2) * int(div_1)
                den_1 = int(den_1) * int(div_2)
                div_1 = int(div_1) * int(div_2)
                div_2 = int(div_1) * int(div_2)
                den_1 = den_1 + den_2
                den_2 = input("ingrese un nuevo numerador de fraccion o escriba = para finalizar: ")
                if den_2 == "=":
                    break
                div_2 = input("ingrese un nuevo denominador de fraccion: ")                
            print(f"el resultado es {den_1}/{div_1}")
        case 2:
            print("ha elegido resta de fracciones")
            den_1 = input("ingrese el numerador de la fraccion: ")
            div_1 = input("ingrese el denominador de la fraccion: ")
            den_2 = input("ingrese el numerador de la otra fraccion: ")
            div_2 = input("ingrese el denominador de la otra fraccion: ")
            while den_2 != "=" or div_2 != "=":
                den_2 = int(den_2) * int(div_1)
                den_1 = int(den_1) * int(div_2)
                div_1 = int(div_1) * int(div_2)
                div_2 = int(div_1) * int(div_2)
                den_1 = den_1 - den_2
                den_2 = input("ingrese un nuevo numerador de fraccion o escriba = para finalizar: ")
                if den_2 == "=":
                    break
                div_2 = input("ingrese un nuevo denominador de fraccion: ")                
            print(f"el resultado es {den_1}/{div_1}")
        case 3:
            print("ha elegido multiplicacion de fracciones")
            den_1 = input("ingrese el numerador de la fraccion: ")
            div_1 = input("ingrese el denominador de la fraccion: ")
            den_2 = input("ingrese el numerador de la otra fraccion: ")
            div_2 = input("ingrese el denominador de la otra fraccion: ")
            while den_2 != "=" or div_2 != "=":
                den_1 = int(den_1) * int(den_2)
                div_1 = int(div_1) * int(div_2)
                den_2 = input("ingrese un nuevo numerador de fraccion o escriba = para finalizar: ")
                if den_2 == "=":
                    break
                div_2 = input("ingrese un nuevo denominador de fraccion: ")                
            print(f"el resultado es {den_1}/{div_1}")
        case 4:
            print("ha elegido division de fracciones")
            den_1 = input("ingrese el numerador de la fraccion: ")
            div_1 = input("ingrese el denominador de la fraccion: ")
            den_2 = input("ingrese el numerador de la otra fraccion: ")
            div_2 = input("ingrese el denominador de la otra fraccion: ")
            while den_2 != "=" or div_2 != "=":
                den_1 = int(den_1) * int(div_2)
                div_1 = int(div_1) * int(den_2)
                den_2 = input("ingrese un nuevo numerador de fraccion o escriba = para finalizar: ")
                if den_2 == "=":
                    break
                div_2 = input("ingrese un nuevo denominador de fraccion: ")                
            print(f"el resultado es {den_1}/{div_1}")
        case other:
            print("ha elegido una opcion incorrecta")
            calculadora_fracciones()
